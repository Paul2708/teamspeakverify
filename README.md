# Teamspeak Verify Plugin (Bungeecord)
--------------------------------------------------
The plugin provides the possibility to link your minecraft account with your teamspeak account.

## Features
* simple setup, no commands are needed
* configure ranks and permissions
* verify teamspeak bot
* ranks will change automaticly
* multiple database types

## Setup
* Put the [`Verify.jar`](https://bitbucket.org/Paul2708/teamspeakverify/downloads/tsverify-1.0-SNAPSHOT.jar) in your plugin folder
* Start the bungeecord server and wait until all files are created
* Stop the bungeecord server
* Set your database type (settings.yml) and add ranks to the system (ranks.yml)
* Start you're bungeecord server again

## How does it work
If a player wants to link his accounts, he has to join the teamspeak server and the minecraft network.
A verify bot will ask for the minecraft username. If the player is online, his accounts will be linked.
If the players' rank change, the teamspeak rank will change as well.

## Configuration
##### ranks.yml
```
visitor_rank: 9
verified_rank: 10
ranks:
- ozeangames.rank.admin 13
- ozeangames.rank.premium 12
- ozeangames.rank.spieler 11
```
##### settings.yml
```
database_type: mysql
```
##### teamspeak.yml
```
ip: localhost
port: 10011
virtual_port: 9987
bot_name: Verify Bot
username: serveradmin
password: Qms5X+Ud
```
##### mysql.yml
```
host: localhost
port: 3306
database: verify
table: verify
user: root
password: password
```
## Pictures
Will be added soon!

## For developers
#### Add new database types
Create a new ```Database``` class..
```java
import de.paul2708.tsverify.VerifyPlugin;
import de.paul2708.tsverify.database.Database;
import de.paul2708.tsverify.database.VerifyPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * This database will only add player data while the server is running and won't save anything.
 *
 * @author Paul2708
 */
public class ExampleDatabase implements Database {

    private List<VerifyPlayer> verifyPlayerList;

    @Override
    public void connect() {
        System.out.println("Connect to databse..");
    }

    @Override
    public void setup() {
        this.verifyPlayerList = new ArrayList<>();
    }

    @Override
    public void addPlayer(String name, UUID uuid, String teamspeakId, String permission) {
        verifyPlayerList.add(new VerifyPlayer(name, uuid, teamspeakId, permission));
    }

    @Override
    public void update(UUID uuid, String newName, String permission) {
        VerifyPlayer player = getVerifyPlayer(uuid);

        player.setName(newName);
        player.setRank(VerifyPlugin.getInstance().getRankManager().getRank(permission));
    }

    @Override
    public void delete(UUID uuid) {
        VerifyPlayer player = getVerifyPlayer(uuid);

        verifyPlayerList.remove(player);
    }

    @Override
    public void disconnect() {
        System.out.println("Database disconnected.");
    }

    @Override
    public VerifyPlayer getVerifyPlayer(String teamspeakId) {
        for (VerifyPlayer player : verifyPlayerList) {
            if (player.getUid().equals(teamspeakId)) {
                return player;
            }
        }

        return null;
    }

    @Override
    public VerifyPlayer getVerifyPlayer(UUID uuid) {
        for (VerifyPlayer player : verifyPlayerList) {
            if (player.getUuid().equals(uuid)) {
                return player;
            }
        }

        return null;
    }

    @Override
    public String getType() {
        return "example";
    }
}
```
... and add it to the list.
```java
package de.paul2708.tsverify.database;

import java.util.ArrayList;
import java.util.List;

public class DatabaseManager {

    private List<Database> databases;

    public DatabaseManager() {
        this.databases = new ArrayList<>();
    }

    public void loadDatabases() {
        this.databases.add(new FileDatabase());
        this.databases.add(new MySQLDatabase());

        this.databases.add(new ExampleDatabase());
    }

    public Database resolve(String type) {
        for (Database other : databases) {
            if (other.getType().equalsIgnoreCase(type)) {
                return other;
            }
        }

        return null;
    }
}
```

## Problems during installation?
Just contact me via Twitter ([@theplayerpaul](https://twitter.com/theplayerpaul)).
Thank you for your attention.