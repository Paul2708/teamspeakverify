import de.paul2708.tsverify.VerifyPlugin;
import de.paul2708.tsverify.database.Database;
import de.paul2708.tsverify.database.VerifyPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * This database will only add player data while the server is running and won't save anything.
 *
 * @author Paul2708
 */
public class ExampleDatabase implements Database {

    private List<VerifyPlayer> verifyPlayerList;

    @Override
    public void connect() {
        System.out.println("Connect to databse..");
    }

    @Override
    public void setup() {
        this.verifyPlayerList = new ArrayList<>();
    }

    @Override
    public void addPlayer(String name, UUID uuid, String teamspeakId, String permission) {
        verifyPlayerList.add(new VerifyPlayer(name, uuid, teamspeakId, permission));
    }

    @Override
    public void update(UUID uuid, String newName, String permission) {
        VerifyPlayer player = getVerifyPlayer(uuid);

        player.setName(newName);
        player.setRank(VerifyPlugin.getInstance().getRankManager().getRank(permission));
    }

    @Override
    public void delete(UUID uuid) {
        VerifyPlayer player = getVerifyPlayer(uuid);

        verifyPlayerList.remove(player);
    }

    @Override
    public void disconnect() {
        System.out.println("Database disconnected.");
    }

    @Override
    public VerifyPlayer getVerifyPlayer(String teamspeakId) {
        for (VerifyPlayer player : verifyPlayerList) {
            if (player.getUid().equals(teamspeakId)) {
                return player;
            }
        }

        return null;
    }

    @Override
    public VerifyPlayer getVerifyPlayer(UUID uuid) {
        for (VerifyPlayer player : verifyPlayerList) {
            if (player.getUuid().equals(uuid)) {
                return player;
            }
        }

        return null;
    }

    @Override
    public String getType() {
        return "example";
    }
}
