package de.paul2708.tsverify.command;

import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import de.paul2708.tsverify.VerifyPlugin;
import de.paul2708.tsverify.database.VerifyPlayer;
import de.paul2708.tsverify.teamspeak.TeamspeakBot;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class UnlinkCommand extends Command {

    // TODO: Edit messages

    private TeamspeakBot bot;

    public UnlinkCommand() {
        super("unlink");

        this.bot = VerifyPlugin.getInstance().getTeamspeakBot();
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if (commandSender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) commandSender;
            VerifyPlayer verifyPlayer = VerifyPlugin.getInstance().getDatabase().getVerifyPlayer(player.getUniqueId());

            if (verifyPlayer != null) {
                Client client = bot.getClient(verifyPlayer.getUid());
                bot.getLinkManager().clear(client);

                ProxyServer.getInstance().getScheduler().runAsync(VerifyPlugin.getInstance(),
                        () -> VerifyPlugin.getInstance().getDatabase().delete(player.getUniqueId())
                );

                player.sendMessage(new TextComponent("§7Du bist nun nicht mehr verlinkt."));
            } else {
                player.sendMessage(new TextComponent("§7Du bist nicht verlinkt. Joine auf den TS um dich zu verlinken."));
            }
        } else {
            System.err.println("Der Befehl ist nur für Spieler.");
        }
    }
}
