package de.paul2708.tsverify.command;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class PermissionCommand extends Command {

    // TODO: Delete command

    public PermissionCommand() {
        super("permission");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if (commandSender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) commandSender;

            player.setPermission(args[0], true);

            player.sendMessage(new TextComponent("Du hast nun die Permission " + args[0]));
        } else {
            System.out.println("Nur Spieler.");
        }
    }
}
