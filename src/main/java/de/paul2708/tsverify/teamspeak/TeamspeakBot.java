package de.paul2708.tsverify.teamspeak;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.ClientProperty;
import com.github.theholywaffle.teamspeak3.api.reconnect.ConnectionHandler;
import com.github.theholywaffle.teamspeak3.api.reconnect.ReconnectStrategy;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ServerGroup;
import de.paul2708.tsverify.VerifyPlugin;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

public class TeamspeakBot {

    private TeamspeakBot instance;

    private TS3Query query;
    private TS3Api api;

    private LinkManager linkManager;

    public void connect() {
        this.instance = this;

        // Create bot config
        TS3Config config = new TS3Config();
        config.setHost(VerifyPlugin.getInstance().getTeamspeakFile().getIp());
        config.setQueryPort(VerifyPlugin.getInstance().getTeamspeakFile().getPort());
        config.setDebugLevel(Level.OFF);
        config.setFloodRate(TS3Query.FloodRate.UNLIMITED);

        config.setConnectionHandler(new ConnectionHandler() {

            @Override
            public void onConnect(TS3Query ts3Query) {
                api = query.getApi();
                api.login(VerifyPlugin.getInstance().getTeamspeakFile().getUserName(),
                        VerifyPlugin.getInstance().getTeamspeakFile().getPassword());
                api.selectVirtualServerById(1);
                api.selectVirtualServerByPort(VerifyPlugin.getInstance().getTeamspeakFile().getVirtualPort());
                api.setNickname(VerifyPlugin.getInstance().getTeamspeakFile().getBotName());

                api.registerAllEvents();
                api.addTS3Listeners(new TeamspeakListener(instance));
            }

            @Override
            public void onDisconnect(TS3Query ts3Query) {

            }
        });

        config.setReconnectStrategy(ReconnectStrategy.exponentialBackoff());

        // Connect bot to server
        query = new TS3Query(config);
        query.connect();

        // Create a new link manager
        this.linkManager = new LinkManager(this);

        // Request
        for (Client client : api.getClients()) {
            if (client.getNickname().equalsIgnoreCase(VerifyPlugin.getInstance().getTeamspeakFile().getBotName())) {
                continue;
            }

            linkManager.request(client);
        }
    }

    public void write(Client client, String message) {
        api.sendPrivateMessage(client.getId(), message);
    }

    public boolean haveSameIP(ProxiedPlayer player, Client client) {
        return player.getAddress().getHostString().equals(client.getIp());
    }

    public void removeGroups(Client client, List<Integer> deleted) {
        List<ServerGroup> allGroups = new ArrayList<>(api.getServerGroupsByClient(client));

        for (ServerGroup group : allGroups) {
            if (deleted.contains(group.getId())) {
                api.removeClientFromServerGroup(group, client);
            }
        }
    }

    public void addGroup(Client client, int groupId) {
        if (hasServerGroup(client, groupId)) {
            return;
        }

        api.addClientToServerGroup(groupId, client.getDatabaseId());
    }

    public boolean hasServerGroup(Client client, int id) {
        for (int group : client.getServerGroups()) {
            if (group == id) {
                return true;
            }
        }

        return false;
    }

    public void changeDescription(Client client, String desc) {
        Map<ClientProperty, String> property = new HashMap<>();
        property.put(ClientProperty.CLIENT_DESCRIPTION, desc);

        api.editClient(client.getId(), property);
    }

    public Client getClient(String uid) {
        for (Client client : api.getClients()) {
            if (client.getUniqueIdentifier().equals(uid)) {
                return client;
            }
        }

        return null;
    }

    public void disconnect() {
        api.logout();
        query.exit();
    }

    public TS3Api getApi() {
        return api;
    }

    public LinkManager getLinkManager() {
        return linkManager;
    }
}