package de.paul2708.tsverify.teamspeak;

import com.github.theholywaffle.teamspeak3.api.TextMessageTargetMode;
import com.github.theholywaffle.teamspeak3.api.event.*;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import de.paul2708.tsverify.VerifyPlugin;
import de.paul2708.tsverify.database.VerifyPlayer;
import de.paul2708.tsverify.rank.TeamspeakRank;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class TeamspeakListener implements TS3Listener {

    // TODO: Edit messages

    private TeamspeakBot bot;

    public TeamspeakListener(TeamspeakBot bot) {
        this.bot = bot;
    }

    @Override
    public void onTextMessage(TextMessageEvent e) {
        if (e.getTargetMode() == TextMessageTargetMode.CLIENT
                && e.getInvokerId() != bot.getApi().whoAmI().getId()) {
            String message = e.getMessage();
            Client client = bot.getApi().getClientByUId(e.getInvokerUniqueId());

            if (!bot.getLinkManager().hasRequested(client)) {
                return;
            }

            ProxiedPlayer target = VerifyPlugin.getInstance().getProxy().getPlayer(message);

            if (target == null || !target.isConnected()) {
                bot.write(client,
                        "Du bist nicht online. Bitte joine auf ozeangames.net und schreibe nochmal deinen Namen.");
                return;
            }
            if (!bot.haveSameIP(target, client)) {
                bot.write(client,
                        "Die IP stimmt nicht mit der verbundenen IP auf dem Netzwerk überein.");
                return;
            }

            // Link player
            TeamspeakRank rank = VerifyPlugin.getInstance().getRankManager().getRank(target);
            System.out.println(rank.getPermission() + " " + rank.getRankId());
            VerifyPlayer player = new VerifyPlayer(target.getName(), target.getUniqueId(),
                    client.getUniqueIdentifier(), rank.getPermission());

            bot.getLinkManager().link(player, client);

            bot.getLinkManager().remove(client);


            ProxyServer.getInstance().getScheduler().runAsync(VerifyPlugin.getInstance(),
                    () -> VerifyPlugin.getInstance().getDatabase().addPlayer(target.getName(), target.getUniqueId(),
                            client.getUniqueIdentifier(), VerifyPlugin.getInstance().getRankManager().getRank(target).getPermission())
            );

            bot.getApi().pokeClient(client.getId(), "Du hast dich erfolgreich mit [B]" + target.getName() + "[/B] verlinkt.");
            target.sendMessage(new TextComponent("§7Du hast dich mit §e" + e.getInvokerName() + " §7verlinkt."));
        }
    }

    @Override
    public void onClientJoin(ClientJoinEvent e) {
        Client client = bot.getApi().getClientByUId(e.getUniqueClientIdentifier());

        bot.getLinkManager().request(client);
    }

    @Override
    public void onClientLeave(ClientLeaveEvent e) {
        Client client = bot.getApi().getClientByUId(e.getInvokerUniqueId());

        bot.getLinkManager().remove(client);
    }

    @Override
    public void onServerEdit(ServerEditedEvent e) {

    }

    @Override
    public void onChannelEdit(ChannelEditedEvent e) {

    }

    @Override
    public void onChannelDescriptionChanged(ChannelDescriptionEditedEvent e) {

    }

    @Override
    public void onClientMoved(ClientMovedEvent e) {

    }

    @Override
    public void onChannelCreate(ChannelCreateEvent e) {

    }

    @Override
    public void onChannelDeleted(ChannelDeletedEvent e) {

    }

    @Override
    public void onChannelMoved(ChannelMovedEvent e) {

    }

    @Override
    public void onChannelPasswordChanged(ChannelPasswordChangedEvent e) {

    }

    @Override
    public void onPrivilegeKeyUsed(PrivilegeKeyUsedEvent e) {

    }
}
