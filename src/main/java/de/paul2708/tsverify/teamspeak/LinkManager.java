package de.paul2708.tsverify.teamspeak;

import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import de.paul2708.tsverify.VerifyPlugin;
import de.paul2708.tsverify.database.VerifyPlayer;
import de.paul2708.tsverify.rank.RankManager;

import java.util.ArrayList;
import java.util.List;

public class LinkManager {

    // TODO: Edit messages

    private TeamspeakBot bot;

    private RankManager rankManager;
    private List<String> requested;

    public LinkManager(TeamspeakBot bot) {
        this.bot = bot;

        this.rankManager = VerifyPlugin.getInstance().getRankManager();
        this.requested = new ArrayList<>();
    }

    public void request(Client client) {
        VerifyPlayer verifyPlayer = VerifyPlugin.getInstance().getDatabase().getVerifyPlayer(client.getUniqueIdentifier());

        if (verifyPlayer == null) {
            // Remove groups and set default group
            clear(client);

            bot.write(client, "Hallo [B]" + client.getNickname() + "[/B]. Du willst deinen Client mit deinem Minecraft-Account verlinken? " +
                    "Dann schreibe mich mit deinem [B]Ingame-Namen[/B] an.");

            requested.add(client.getUniqueIdentifier());
        } else {
            link(verifyPlayer, client);
        }
    }

    public void clear(Client client) {
        List<Integer> deletedRanks = new ArrayList<>(rankManager.getRankIds());
        deletedRanks.add(VerifyPlugin.getInstance().getRankFile().getVerifiedRank());

        bot.removeGroups(client, deletedRanks);
        bot.addGroup(client, VerifyPlugin.getInstance().getRankFile().getVisitorRank());

        bot.changeDescription(client, "nicht verlinkt");
    }

    public void link(VerifyPlayer verifyPlayer, Client client) {
        List<Integer> deletedRanks = new ArrayList<>(rankManager.getRankIds());
        deletedRanks.remove(new Integer(verifyPlayer.getRank().getRankId()));
        deletedRanks.add(VerifyPlugin.getInstance().getRankFile().getVisitorRank());

        bot.removeGroups(client, deletedRanks);
        bot.addGroup(client, VerifyPlugin.getInstance().getRankFile().getVerifiedRank());
        bot.addGroup(client, verifyPlayer.getRank().getRankId());

        bot.changeDescription(client, "Minecraft-Acc.: " + verifyPlayer.getName());
    }

    public void link(VerifyPlayer verifyPlayer) {
        Client client = bot.getClient(verifyPlayer.getUid());

        link(verifyPlayer, client);
    }

    public boolean hasRequested(Client client) {
        return requested.contains(client.getUniqueIdentifier());
    }

    public void remove(Client client) {
        requested.remove(client.getUniqueIdentifier());
    }
}