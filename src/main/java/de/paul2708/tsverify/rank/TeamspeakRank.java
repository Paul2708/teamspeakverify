package de.paul2708.tsverify.rank;

public class TeamspeakRank {

    private String permission;
    private int rankId;

    public TeamspeakRank(String permission, int rankId) {
        this.permission = permission;
        this.rankId = rankId;
    }

    public String getPermission() {
        return permission;
    }

    public int getRankId() {
        return rankId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !getClass().equals(obj.getClass())) {
            return false;
        }

        TeamspeakRank rank = (TeamspeakRank) obj;

        return rankId == rank.getRankId();
    }

    @Override
    public String toString() {
        return permission + " " + rankId;
    }
}
