package de.paul2708.tsverify.rank;

import de.paul2708.tsverify.VerifyPlugin;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.List;

public class RankManager {

    private List<TeamspeakRank> ranks;
    private List<Integer> rankIds;

    public RankManager() {
        this.ranks = new ArrayList<>();
        this.rankIds = new ArrayList<>();
    }

    public void resolve() {
        for (String rank : VerifyPlugin.getInstance().getRankFile().getRanks()) {
            ranks.add(new TeamspeakRank(rank.split(" ")[0], Integer.parseInt(rank.split(" ")[1])));
        }

        for (TeamspeakRank rank : ranks) {
            rankIds.add(rank.getRankId());
        }
    }

    public TeamspeakRank getRank(String permission) {
        for (TeamspeakRank rank : ranks) {
            if (rank.getPermission().equalsIgnoreCase(permission)) {
                return rank;
            }
        }

        return null;
    }

    public TeamspeakRank getRank(ProxiedPlayer player) {
        for (TeamspeakRank rank : ranks) {
            if (player.hasPermission(rank.getPermission())) {
                return rank;
            }
        }

        return null;
    }

    public List<Integer> getRankIds() {
        return rankIds;
    }
}
