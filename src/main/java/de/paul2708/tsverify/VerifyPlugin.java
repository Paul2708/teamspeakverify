package de.paul2708.tsverify;

import de.paul2708.tsverify.command.PermissionCommand;
import de.paul2708.tsverify.command.UnlinkCommand;
import de.paul2708.tsverify.config.MySQLFile;
import de.paul2708.tsverify.config.RankFile;
import de.paul2708.tsverify.config.SettingsFile;
import de.paul2708.tsverify.config.TeamspeakFile;
import de.paul2708.tsverify.database.Database;
import de.paul2708.tsverify.database.DatabaseManager;
import de.paul2708.tsverify.listener.ServerConnectedListener;
import de.paul2708.tsverify.rank.RankManager;
import de.paul2708.tsverify.teamspeak.TeamspeakBot;
import net.md_5.bungee.api.plugin.Plugin;

public class VerifyPlugin extends Plugin {

    // TODO: Edit messages

    private static VerifyPlugin instance;

    private SettingsFile settingsFile;
    private TeamspeakFile teamspeakFile;
    private RankFile rankFile;
    private MySQLFile mySQLFile;

    private RankManager rankManager;

    private TeamspeakBot teamspeakBot;

    private Database database;

    @Override
    public void onLoad() {
        VerifyPlugin.instance = this;
    }

    @Override
    public void onEnable() {
        // Create files
        this.settingsFile = new SettingsFile();
        this.settingsFile.load();

        this.teamspeakFile = new TeamspeakFile();
        this.teamspeakFile.load();

        this.rankFile = new RankFile();
        this.rankFile.load();

        this.mySQLFile = new MySQLFile();
        this.mySQLFile.load();

        // Rank manager
        this.rankManager = new RankManager();
        this.rankManager.resolve();

        // Database
        DatabaseManager databaseManager = new DatabaseManager();
        databaseManager.loadDatabases();

        Database database = databaseManager.resolve(settingsFile.getDatabase());
        if (database == null) {
            System.err.println("Failed to bind database type.");
            getProxy().stop();
            return;
        }

        this.database = database;
        this.database.connect();
        this.database.setup();

        // Teamspeak bot
        this.teamspeakBot = new TeamspeakBot();
        this.teamspeakBot.connect();

        // Register
        registerListener();
        registerCommands();
    }

    @Override
    public void onDisable() {
        this.teamspeakBot.disconnect();

        if (database != null) {
            database.disconnect();
        }
    }

    private void registerListener() {
        getProxy().getPluginManager().registerListener(this, new ServerConnectedListener());
    }

    private void registerCommands() {
        getProxy().getPluginManager().registerCommand(this, new UnlinkCommand());
        getProxy().getPluginManager().registerCommand(this, new PermissionCommand());
    }

    public static VerifyPlugin getInstance() {
        return instance;
    }

    public TeamspeakFile getTeamspeakFile() {
        return teamspeakFile;
    }

    public RankFile getRankFile() {
        return rankFile;
    }

    public MySQLFile getMySQLFile() {
        return mySQLFile;
    }

    public RankManager getRankManager() {
        return rankManager;
    }

    public Database getDatabase() {
        return database;
    }

    public TeamspeakBot getTeamspeakBot() {
        return teamspeakBot;
    }
}