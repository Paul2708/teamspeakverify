package de.paul2708.tsverify.listener;

import de.paul2708.tsverify.VerifyPlugin;
import de.paul2708.tsverify.database.VerifyPlayer;
import de.paul2708.tsverify.rank.TeamspeakRank;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ServerConnectedListener implements Listener {

    // TODO: Edit messages and remove permission

    @EventHandler
    public void onPlayerJoin(ServerConnectedEvent event) {
        ProxiedPlayer player = event.getPlayer();

        player.setPermission("ozeangames.rank.spieler", true);

        VerifyPlayer verifyPlayer = VerifyPlugin.getInstance().getDatabase().getVerifyPlayer(player.getUniqueId());

        if (verifyPlayer != null) {
            // Check name
            String name = player.getName();
            if (!verifyPlayer.getName().equals(name)) {
                name = player.getName();
            }

            // Check rank
            TeamspeakRank rank = verifyPlayer.getRank();
            TeamspeakRank other = VerifyPlugin.getInstance().getRankManager().getRank(player);

            if (other == null) {
                player.sendMessage(new TextComponent("Please contact an admin. You do not have a permission."));
                return;
            }

            if (!other.equals(rank)) {
                rank = VerifyPlugin.getInstance().getRankManager().getRank(player);
            }

            verifyPlayer.setName(name);
            verifyPlayer.setRank(rank);

            VerifyPlugin.getInstance().getTeamspeakBot().getLinkManager().link(verifyPlayer);
            ProxyServer.getInstance().getScheduler().runAsync(VerifyPlugin.getInstance(),
                    () -> VerifyPlugin.getInstance().getDatabase().update(player.getUniqueId(), verifyPlayer.getName(), verifyPlayer.getRank().getPermission())
            );
        }
    }
}