package de.paul2708.tsverify.config;

import net.md_5.bungee.config.Configuration;

public class MySQLFile extends ConfigurationFile {

    public MySQLFile() {
        super("mysql.yml");
    }

    public void createDefaultValue() {
        Configuration configuration = getConfiguration();

        configuration.set("host", "localhost");
        configuration.set("port", 3306);
        configuration.set("database", "database");
        configuration.set("table", "verify");
        configuration.set("user", "user");
        configuration.set("password", "password");

        save();
    }

    public String getHost() {
        return getConfiguration().getString("host");
    }

    public int getPort() {
        return getConfiguration().getInt("port");
    }

    public String getDatabase() {
        return getConfiguration().getString("database");
    }

    public String getTable() {
        return getConfiguration().getString("table");
    }

    public String getUser() {
        return getConfiguration().getString("user");
    }

    public String getPassword() {
        return getConfiguration().getString("password");
    }
}
