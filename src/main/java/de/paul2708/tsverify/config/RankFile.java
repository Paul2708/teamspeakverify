package de.paul2708.tsverify.config;

import de.paul2708.tsverify.rank.TeamspeakRank;
import net.md_5.bungee.config.Configuration;

import java.util.ArrayList;
import java.util.List;

public class RankFile extends ConfigurationFile {

    public RankFile() {
        super("ranks.yml");
    }

    @Override
    public void createDefaultValue() {
        Configuration configuration = getConfiguration();

        configuration.set("visitor_rank", 1234);
        configuration.set("verified_rank", 1337);

        List<String> list = new ArrayList<>();
        list.add(new TeamspeakRank("ozeangames.rank.admin", 1234).toString());
        list.add(new TeamspeakRank("ozeangames.rank.premium", 2345).toString());
        list.add(new TeamspeakRank("ozeangames.rank.spieler", 3456).toString());

        configuration.set("ranks", list);

        save();
    }

    public int getVisitorRank() {
        return getConfiguration().getInt("visitor_rank");
    }

    public int getVerifiedRank() {
        return getConfiguration().getInt("verified_rank");
    }

    public List<String> getRanks() {
        return getConfiguration().getStringList("ranks");
    }
}