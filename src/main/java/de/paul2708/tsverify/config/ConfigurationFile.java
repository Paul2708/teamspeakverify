package de.paul2708.tsverify.config;

import de.paul2708.tsverify.VerifyPlugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public abstract class ConfigurationFile {

    // TODO: Edit error message

    private String name;

    private Configuration configuration;

    public ConfigurationFile(String name) {
        this.name = name;
    }

    public void load() {
        try {

            boolean created = createFiles();

            this.configuration = ConfigurationProvider.getProvider(YamlConfiguration.class)
                    .load(new File(VerifyPlugin.getInstance().getDataFolder(), name));

            if (created) {
                createDefaultValue();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public abstract void createDefaultValue();

    public void save() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class)
                    .save(configuration, new File(VerifyPlugin.getInstance().getDataFolder(), name));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    private boolean createFiles() throws IOException {
        boolean created = false;

        // Create files
        File file = VerifyPlugin.getInstance().getDataFolder();

        if (!file.exists()) {
            file.mkdir();
            created = true;
        }

        file = new File(VerifyPlugin.getInstance().getDataFolder(), name);
        if (!file.exists()) {
            file.createNewFile();
            created = true;
        }

        return created;
    }
}