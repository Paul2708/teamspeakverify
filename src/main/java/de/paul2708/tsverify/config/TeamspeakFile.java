package de.paul2708.tsverify.config;

import net.md_5.bungee.config.Configuration;

public class TeamspeakFile extends ConfigurationFile {

    public TeamspeakFile() {
        super("teamspeak.yml");
    }

    public void createDefaultValue() {
        Configuration configuration = getConfiguration();

        configuration.set("ip", "localhost");
        configuration.set("port", 9987);
        configuration.set("virtual_port", 9987);

        configuration.set("bot_name", "Verify Bot");

        configuration.set("username", "serveradmin");
        configuration.set("password", "password");

        save();
    }

    public String getIp() {
        return getConfiguration().getString("ip");
    }

    public int getPort() {
        return getConfiguration().getInt("port");
    }

    public int getVirtualPort() {
        return getConfiguration().getInt("virtual_port");
    }

    public String getBotName() {
        return getConfiguration().getString("bot_name");
    }

    public String getUserName() {
        return getConfiguration().getString("username");
    }

    public String getPassword() {
        return getConfiguration().getString("password");
    }
}
