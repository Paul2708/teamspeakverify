package de.paul2708.tsverify.config;

import net.md_5.bungee.config.Configuration;

public class SettingsFile extends ConfigurationFile {

    public SettingsFile() {
        super("settings.yml");
    }

    public void createDefaultValue() {
        Configuration configuration = getConfiguration();

        configuration.set("database_type", "file");

        save();
    }

    public String getDatabase() {
        return getConfiguration().getString("database_type");
    }
}
