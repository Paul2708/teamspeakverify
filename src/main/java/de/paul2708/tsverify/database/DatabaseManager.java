package de.paul2708.tsverify.database;

import java.util.ArrayList;
import java.util.List;

public class DatabaseManager {

    // TODO: Add more databases

    private List<Database> databases;

    public DatabaseManager() {
        this.databases = new ArrayList<>();
    }

    public void loadDatabases() {
        this.databases.add(new FileDatabase());
        this.databases.add(new MySQLDatabase());
    }

    public Database resolve(String type) {
        for (Database other : databases) {
            if (other.getType().equalsIgnoreCase(type)) {
                return other;
            }
        }

        return null;
    }
}