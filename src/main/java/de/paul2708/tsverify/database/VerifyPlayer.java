package de.paul2708.tsverify.database;

import de.paul2708.tsverify.VerifyPlugin;
import de.paul2708.tsverify.rank.TeamspeakRank;

import java.util.UUID;

public class VerifyPlayer {

    private String name;
    private UUID uuid;
    private String uid;
    private TeamspeakRank rank;

    public VerifyPlayer(String name, UUID uuid, String uid, String permission) {
        this.name = name;
        this.uuid = uuid;
        this.uid = uid;
        this.rank = VerifyPlugin.getInstance().getRankManager().getRank(permission);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRank(TeamspeakRank rank) {
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getUid() {
        return uid;
    }

    public TeamspeakRank getRank() {
        return rank;
    }
}