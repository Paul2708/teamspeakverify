package de.paul2708.tsverify.database;

import de.paul2708.tsverify.VerifyPlugin;
import de.paul2708.tsverify.config.ConfigurationFile;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FileDatabase extends ConfigurationFile implements Database {

    private List<VerifyPlayer> verifyPlayerList;

    public FileDatabase() {
        super("storage.yml");
    }

    @Override
    public void createDefaultValue() {
        getConfiguration().set("players", new ArrayList<>());

        save();
    }

    @Override
    public void connect() {
        load();

        this.verifyPlayerList = transferToVerify(getConfiguration().getStringList("players"));
    }

    @Override
    public void setup() {
        // Nothing to do
    }

    @Override
    public void addPlayer(String name, UUID uuid, String teamspeakId, String permission) {
        verifyPlayerList.add(new VerifyPlayer(name, uuid, teamspeakId, permission));

        List<String> list = getConfiguration().getStringList("players");
        list.add(name + ";" + uuid.toString() + ";" + teamspeakId + ";" + permission);

        getConfiguration().set("players", list);
        save();
    }

    @Override
    public void update(UUID uuid, String newName, String permission) {
        VerifyPlayer player = getVerifyPlayer(uuid);

        player.setName(newName);
        player.setRank(VerifyPlugin.getInstance().getRankManager().getRank(permission));

        getConfiguration().set("players", transferToString(verifyPlayerList));
        save();
    }

    @Override
    public void delete(UUID uuid) {
        VerifyPlayer player = getVerifyPlayer(uuid);

        verifyPlayerList.remove(player);

        getConfiguration().set("players", transferToString(verifyPlayerList));
        save();
    }

    @Override
    public void disconnect() {
        save();
    }

    @Override
    public VerifyPlayer getVerifyPlayer(String teamspeakId) {
        for (VerifyPlayer player : verifyPlayerList) {
            if (player.getUid().equals(teamspeakId)) {
                return player;
            }
        }

        return null;
    }

    @Override
    public VerifyPlayer getVerifyPlayer(UUID uuid) {
        for (VerifyPlayer player : verifyPlayerList) {
            if (player.getUuid().equals(uuid)) {
                return player;
            }
        }

        return null;
    }

    @Override
    public String getType() {
        return "file";
    }

    private List<VerifyPlayer> transferToVerify(List<String> list) {
        List<VerifyPlayer> verifyPlayers = new ArrayList<>();

        for (String line : list) {
            String[] array = line.split(";");

            VerifyPlayer player = new VerifyPlayer(array[0], UUID.fromString(array[1]), array[2], array[3]);
            verifyPlayers.add(player);
        }

        return verifyPlayers;
    }

    private List<String> transferToString(List<VerifyPlayer> list) {
        List<String> content = new ArrayList<>();

        for (VerifyPlayer player : list) {
            content.add(player.getName() + ";" + player.getUuid().toString() + ";" + player.getUid() + ";" + player.getRank().getPermission());
        }

        return content;
    }
}
