package de.paul2708.tsverify.database;

import de.paul2708.tsverify.VerifyPlugin;

import java.sql.*;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class MySQLDatabase implements Database {

    // TODO: Edit messages

    private String host;
    private int port;
    private String database;
    private String table;
    private String user;
    private String password;

    private Connection connection;

    private List<VerifyPlayer> list;

    public MySQLDatabase() {
        this.host = VerifyPlugin.getInstance().getMySQLFile().getHost();
        this.database = VerifyPlugin.getInstance().getMySQLFile().getDatabase();
        this.table = VerifyPlugin.getInstance().getMySQLFile().getTable();
        this.port = VerifyPlugin.getInstance().getMySQLFile().getPort();
        this.user = VerifyPlugin.getInstance().getMySQLFile().getUser();
        this.password = VerifyPlugin.getInstance().getMySQLFile().getPassword();
    }

    @Override
    public void connect() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            this.connection = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database + "?user=" + this.user + "&password=" + this.password + "&autoReconnect=true");
            System.out.println("SQL: Verbindung zur Datenbank '" + this.database + "' hergestellt");
        } catch (SQLException e) {
            System.err.println("SQL: Verbindung zur Datenbank '" + this.database + "' konnte nicht hergestellt werden");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.err.println("SQL: MySQL-Treiber wurde nicht gefunden.");
        }
    }

    @Override
    public void setup() {
        String query = "CREATE TABLE IF NOT EXISTS `" + table + "` ("
                + "`name` varchar(200) NOT NULL,"
                + "`uuid` varchar(200) NOT NULL,"
                + "`uid` varchar(200) NOT NULL,"
                + "`permission` varchar(200) NOT NULL,"
                + "UNIQUE KEY `uuid` (`uuid`)) "
                + "ENGINE=InnoDB DEFAULT CHARSET=latin1;";

        try {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        this.list = new CopyOnWriteArrayList<>();
        try {
            ResultSet resultSet = connection.prepareStatement("SELECT * FROM `" + table + "`").executeQuery();

            while (resultSet.next()) {
                String name = resultSet.getString("name");
                String uuid = resultSet.getString("uuid");
                String uid = resultSet.getString("uid");
                String permission = resultSet.getString("permission");

                VerifyPlayer player = new VerifyPlayer(name, UUID.fromString(uuid), uid, permission);
                list.add(player);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addPlayer(String name, UUID uuid, String uid, String permission) {
        VerifyPlayer player = new VerifyPlayer(name, uuid, uid, permission);
        list.add(player);

        try {
            PreparedStatement update = connection.prepareStatement(
                    "INSERT IGNORE INTO `" +  table + "` ("
                            + "`name`, `uuid`, `uid`, `permission`"
                            + ") VALUES ("
                            + "?, ?, ?, ?"
                            + ")");

            update.setString(1, name);
            update.setString(2, uuid.toString());
            update.setString(3, uid);
            update.setString(4, permission);

            update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(UUID uuid, String newName, String permission) {
        for (VerifyPlayer player : list) {
            if (player.getUuid().equals(uuid)) {
                player.setName(newName);
                player.setRank(VerifyPlugin.getInstance().getRankManager().getRank(permission));
                break;
            }
        }

        try {
            PreparedStatement update = connection.prepareStatement(
                    "UPDATE " + table + " SET name = ? , permission = ? WHERE uuid = ?");

            update.setString(1, newName);
            update.setString(2, permission);
            update.setString(3, uuid.toString());

            update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(UUID uuid) {
        VerifyPlayer player = null;
        for (VerifyPlayer other : list) {
            if (other.getUuid().equals(uuid)) {
                player = other;
                break;
            }
        }
        if (player != null) {
            list.remove(player);
        }

        try {
            PreparedStatement update = connection.prepareStatement(
                    "DELETE FROM " + table + " WHERE uuid = ?");

            update.setString(1, uuid.toString());

            update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void disconnect() {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public VerifyPlayer getVerifyPlayer(String teamspeakId) {
        for (VerifyPlayer player : list) {
            if (player.getUid().equals(teamspeakId)) {
                return player;
            }
        }

        return null;
    }

    @Override
    public VerifyPlayer getVerifyPlayer(UUID uuid) {
        for (VerifyPlayer player : list) {
            if (player.getUuid().equals(uuid)) {
                return player;
            }
        }

        return null;
    }

    @Override
    public String getType() {
        return "mysql";
    }
}
