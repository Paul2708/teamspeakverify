package de.paul2708.tsverify.database;

import java.util.UUID;

public interface Database {

    void connect();

    void setup();

    void addPlayer(String name, UUID uuid, String uid, String permission);

    void update(UUID uuid, String newName, String permission);

    void delete(UUID uuid);

    void disconnect();

    VerifyPlayer getVerifyPlayer(String teamspeakId);

    VerifyPlayer getVerifyPlayer(UUID uuid);

    String getType();
}